﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FranceVacancies
{
    class Bookning
    {
        public static int NumberOfOrders = 0;

        public int OrderNumber { get; }
        public int ApartmentId { get; set; }
        public int UserId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDataTime { get; set; }

        public Bookning(int apartmentId, int userId, DateTime startDataTime, DateTime endDataTime)
        {
	        OrderNumber = 1 + NumberOfOrders;
            
            ApartmentId = apartmentId;
            UserId = userId;
            StartDateTime = startDataTime;
            EndDataTime = endDataTime;

	        NumberOfOrders++;
        }
    }
}
