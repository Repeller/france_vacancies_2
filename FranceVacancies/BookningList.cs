﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FranceVacancies
{
    class BookningList
    {
        public List<Bookning> Booknings { get; set; }

        public BookningList()
        {
            Booknings = new List<Bookning>();
        }

        public void AddOneBookning(Bookning tempBookning)
        {
            Booknings.Add(tempBookning);
        }
        public void AddOneBookning(int apartmentId, int userId, DateTime startDateTime, DateTime endDataTime)
        {
            Bookning tempBookning = new Bookning(apartmentId, userId, startDateTime, endDataTime);
            Booknings.Add(tempBookning);
        }



        public void PrintOutAll()
        {
            foreach (Bookning book in Booknings)
            {
                Console.WriteLine($"booking order number {book.OrderNumber} - the user id  '{book.UserId}' the apartment id '{book.ApartmentId}', the user will be in it from '{book.StartDateTime}' to '{book.EndDataTime}'");
            }
        }


    }
}
