﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FranceVacancies
{
	internal class BasicUser
	{
		public static int CountOfUsers = 0;

		public int Id { get; }
		public string Name { get; set; }
		public string Email { get; set; }

		public string UserName { get; }
		public string Password { get; }

		//public string Address { get; set; }
		//public string PhoneNumber { get; set; }
		//public string CardInfo { get; set; }

		public BasicUser(string name, string email, string userName, string password)
		{
			Id = 1 + CountOfUsers;

			Name = name;
			Email = email;
			UserName = userName;
			Password = password;

			CountOfUsers++;
		}
	}
}
