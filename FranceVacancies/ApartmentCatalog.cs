﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FranceVacancies
{
    class ApartmentCatalog
    {
        public List<Apartment> Apartments { get; set; }

        public ApartmentCatalog()
        {
            Apartments = new List<Apartment>();
        }

        public void AddOneApartment(Apartment tempApartment)
        {
            Apartments.Add(tempApartment);
        }

        public void PrintAll()
        {
            foreach (Apartment tempApartment in Apartments)
            {
                Console.WriteLine($"id '{tempApartment.Id}' (available : {tempApartment.Available} ) - type {tempApartment.Type} , address '{tempApartment.Address}'");
                Console.WriteLine(
                    $"number of rooms '{tempApartment.NumberOfRooms}' size '{tempApartment.Size}' - price {tempApartment.Price}");
                Console.WriteLine($"Description: {tempApartment.Description}");
                Console.WriteLine("< ---------- ------------ >");

            }
        }

        public void LoadApartments()
        {
			
            Apartments.Add(new Apartment(Apartment.ApartmentType.house, "hell", 50.5, 50.5, 200.5, "big fucking house"));

            //Apartments.Add(new Apartment("bad", "hell 666", true, 25.5, 3.5, 24000, "bad deal"));
            //Apartments.Add(new Apartment("okay", "girl street 753", true, 25.5, 3.5, 24000, "okay deal"));
            //Apartments.Add(new Apartment("dark", "bat cave", true, 25.5, 3.5, 24000, "it is really dark in this crave"));
            //Apartments.Add(new Apartment("light", "sky-street 641", true, 25.5, 3.5, 24000, "too much light, I can't see!"));
            //Apartments.Add(new Apartment("pink", "gay street 690", true, 25.5, 3.5, 24000, "Í didn't go there, and I never will"));
        }
    }
}
